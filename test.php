<?php

function xo($str)
{
    $x = $str;
    $o = $str;
    if (substr_count($x, 'x') == substr_count($o, 'o')) {
        echo "Benar";
    } else {
        echo "Salah";
    }
}

echo xo('xoxoxo'); // "Benar"
echo "<br>";
echo xo('oxooxo'); // "Salah"
echo "<br>";
echo xo('oxo'); // "Salah"
echo "<br>";
echo xo('xxooox'); // "Benar"
echo "<br>";
echo xo('xoxooxxo'); // "Benar"



echo "<br>";
function pagar_bintang($integer)
{
    for ($i = 0; $i < $integer; $i++) {
        echo "";
        for ($j = 0; $j < $integer; $j++) {
            if ($i % 2 == 0) {
                echo "#";
            } else {
                echo "*";
            }
        }
        echo "<br>";
    }
}

echo pagar_bintang(5);
echo "<br>";
echo pagar_bintang(8);
echo "<br>";
echo pagar_bintang(10);
